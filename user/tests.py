import math

from django.contrib.auth.models import User
from django.test import TestCase

from projects.models import ProjectCategory
from user.models import update_user_rating, Profile


class UpdateUserRatingTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='john',
            first_name='john',
            last_name='johnson',
            email='jlennon@beatles.com'
        )
        self.user.save()
        self.profile = self.user.profile

    def test_update_user_rating_give_rating(self):
        user_rating = update_user_rating(self.profile.user_id, 2, '1')
        self.assertTrue(user_rating.rating == 2)

    def test_update_user_rating_update_rating_on_task(self):
        user_with_rating = self.profile
        user_with_rating.rating = 3
        user_with_rating.given_rating_votes = {'1': 3}
        user_with_rating.save()
        user_rating = update_user_rating(user_with_rating.id, 5, '1')
        self.assertTrue(user_rating.rating == 5)

    def test_update_user_update_rating(self):
        user_with_rating = self.profile
        user_with_rating.rating = 3
        user_with_rating.given_rating_votes = {'1': 3}
        user_with_rating.save()
        user_rating = update_user_rating(user_with_rating.id, 5, '2')
        # 8/2=4
        self.assertTrue(user_rating.rating == 4)

